<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     
    protected $fillable = [
        'komentar','movie_id'
    ];

    public function movie(){
        return $this->belongsTo('App\Movie','movie_id');
        //movie_id = fk
    }
}
