<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //
    protected $fillable = [
        'title', 'synopsis','director','daterealese','image'
    ];
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function bioskop()
    {
        return $this->hasMany('App\Bioskop');
    }
}
