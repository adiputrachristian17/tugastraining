<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bioskop extends Model
{
    //
    protected $fillable =[
        'nama', 'lokasi','movie_id'
    ];

    public function movie(){
        return $this->belongsTo('App\Movie','movie_id');
    }
}
