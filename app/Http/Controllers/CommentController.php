<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Movie;
class CommentController extends Controller
{
    //
    // public function index(){
    //     return view('comment');
    // }
    public function index(){
        $comments = Comment::paginate(6);
        return view('comment',compact('comments'));
    }

    public function create(){

        $movies = Movie::all();
        return view('commentform',compact('movies'));
    }

    public function store(CommentRequest $request){
        Comment::create([
            'movie_id'=>$request->movie_id,
            'komentar'=>$request->komentar
        ]);
        return redirect('comment');
    }

    public function delete($id){
        Comment::destroy($id);
        return back()->with('Success', 'Success Delete Movie Data');
    }
}
