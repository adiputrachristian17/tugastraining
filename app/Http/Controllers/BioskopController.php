<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bioskop;
use App\Movie;
use App\Http\Requests\BioskopRequest;
class BioskopController extends Controller
{
    //
    public function index(){

        $bioskops = Bioskop::paginate(6);
        return view('bioskop',compact('bioskops'));
    }

    public function create(){
        $movies = Movie::all();
        return view('bioskopform',compact('movies'));
    }

    public function store(BioskopRequest $request){
        //dd($request->all());
        Bioskop::create([
            'movie_id'=>$request->movie_id,
            'nama'=>$request->nama,
            'lokasi'=>$request->lokasi
        ]);
        return redirect('bioskop');
    }

    public function edit($id){
        $bioskop= Bioskop::findOrFail($id);
        return view('editbioskop',compact('bioskop'));
    }

    public function update(BioskopRequest $request,$id){
        Bioskop::findOrFail($id)->update([
            'nama'=>$request->nama,
            'lokasi'=>$request->lokasi,
            
        ]);
        return redirect('/bioskop')->with('success','Success Update Bioskop Data');
    }

    public function delete($id){
        Bioskop::destroy($id);
        return back()->with('Success', 'Succes Delete Bioskop Data');
    }
}
