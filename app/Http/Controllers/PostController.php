<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use DB;
use Validator;
use App\Http\Requests\PostRequest;
class PostController extends Controller
{
    //
    public function index(){

        //  $posts = Post::orderBy('title')->get();
        //  dd($posts);
        //$posts = Post::take(10)->get();
        $posts = Post::paginate(6);
          return view('post',compact('posts'));
      }

      public function store(PostRequest $request) //$request bisa custom
      {
        //1. caara traditional
        // $post = new Post;
        // $post->title = $request->title;
        // $post->description  = $request->description;
        // $post->author = $request->author;
        // $post->save(); //buat ngesave data yg diinput
        // echo 'succes';

        //2. pake query builder
        // DB::table('posts')->insert([
        //     'title'=>$request->title,
        //     'description'=>$request->description,
        //     'author' =>$request->author    
        // ]);
        // echo 'use query builder';
        //post disini ikutin nama tabel

        //1.cara validate
        // $validator = Validator::make($request->all(),[
            //     'title' => 'required|min:5', //title disini ikutin name di view
            //     'description' =>'required|max:100',
            //     'author'=>'required'
            // ]);
            // if($validator->fails())
            // {
                //     return back()->withErrors($validator->errors())->withInput();
                // }
                
                //2. cara validate
                // $request->validate([
                    //     'title' => 'required|min:5', //title disini ikutin name di view
                    //     'description' =>'required',
                    //     'author'=>'required'
                    // ]);
        //3. Pake Eloquent
                    Post::create([
                        'title'=>$request->title,
                        'description'=>$request->description,
                        'author' =>$request->author
        ]);
        //echo 'with eloquent';
        return redirect('/post')->with('success','Succes Input Post Data');
        //succes = session , isinya section
    }
    
}
