<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
Use Validator;
use App\Http\Requests\MovieRequest;
class MovieController extends Controller
{
    //
    public function index(){
        $movies = Movie::paginate(6);
        return view('movie',compact('movies'));
    }

    public function create(){
        return view('movieform');
    }

    public function store(MovieRequest $request){
        //dd($request->all());

        

        Movie::create([
            'title'=>$request->title,
            'synopsis'=>$request->synopsis,
            'director'=>$request->director,
            'daterealese'=>$request->daterealese,
            'image'=>$this->uploadImage($request->file('image'))
        ]);
        return redirect('movie');
    }

    public function uploadImage($image){
        $fileNameWithExt = $image->getClientOriginalName();
        $image->storeAs('public/image',$fileNameWithExt);
        return $fileNameWithExt;
    }

    //update dan delete

    public function edit($id){
        $movie= Movie::findOrFail($id);
        return view('edit-movie',compact('movie'));
    }

    public function update(MovieRequest $request,$id){
        Movie::findOrFail($id)->update([
            'title'=>$request->title,
            'synopsis'=>$request->synopsis,
            'director'=>$request->director,
            'daterealese'=>$request->daterealese,
            'image'=>$this->uploadImage($request->file('image'))
        ]);
        return redirect('/movie')->with('success','Success Update Movie Data');
    }

    public function show($id){
        $movie = Movie::findorFail($id);
        return view('showmovie',compact('movie'));
    }
    public function delete($id){
        Movie::destroy($id);
        return back()->with('Success', 'Succes Delete Movie Data');
    }
}
