<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'Star Wars',
            'description' => 'Perang Bintang',
            'author' => 'J.J Abram'
        ]);

        DB::table('posts')->insert([
            'title' => 'Jurassic Park',
            'description' => 'Dinosaur',
            'author' => 'Steven Speilberg'
        ]);
    }
}
