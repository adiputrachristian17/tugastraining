<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'komentar'=> 'Kent martin sedang makan nasi goreng'
        ]);
        DB::table('comments')->insert([
            'komentar'=> 'Randi adiel sedang mencuci piring'
        ]);
    }
}
