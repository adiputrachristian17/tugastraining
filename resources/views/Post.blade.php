<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>

    <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{$messages}}</li>
                        @endforeach 
                    </ul>
                </div>
            @endif
            <form action="{{route('post.add')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input name="title" type="text" class="form-control" id="title">
            </div>
            <div class="form-group">
                <label for="desc">Description:</label>
                <input name="description" type="text" class="form-control" id="desc">
            </div>
            <div class="form-group">
                <label for="author">Author:</label>
                <input name="author" type="text" class="form-control" id="author">
            </div>
            <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            </form>
            
    </div>
    <div class="container">
        @if (Session::has('success'))
            <div class="alert alert-success">
                <p> {{Session::get('success')}}</p>
            </div>
        @endif
        <table class="table">
            <thead>
                <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Author</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                <tr>
                <td> {{$post->title}}</td>
                <td> {{$post->description}}</td>
                <td> {{$post->author}}</td>
                <td>
                    <button class="btn btn-danger">Delete </button>
                </td>
                <td>
                    <button class="btn btn-succes">
                    Edit
                    </button>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$posts->links()}}
    </div>
</body>
</html>