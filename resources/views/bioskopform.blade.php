<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
<div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{$messages}}</li>
                        @endforeach 
                    </ul>
                </div>
            @endif
            <form action="{{route('bioskop.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="bioskop">Movie Title</label>
                <select class="form-control" name="movie_id" id="">
                    @foreach($movies as $movie)
                    <option value="{{$movie->id}}">{{$movie->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="nama">Nama Bioskop:</label>
                <input name="nama" type="text" class="form-control" id="title">
            </div>
            <div class="form-group">
                <label for="lokasi">Lokasi:</label>
                <input name="lokasi" type="text" class="form-control" id="desc">
            </div>
            <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            </form>
            
    </div>
</body>
</html>