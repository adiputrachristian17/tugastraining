<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                <th>Movie</th>
                </tr>
            </thead>
            <tbody>
                @foreach($movies as $movie)
                <tr>
                <td> {{$movie->title}}</td>
                <td> {{$movie->synopsis}}</td>
                <td> {{$movie->director}}</td>
                <td> {{$movie->daterealese}}</td>
                <td>
                    <img src="{{asset('storage/image/'.$movie->image)}}" alt="" height="100" width="100">
                </td>
                <td>
                    <form action="{{route('movie.delete', $movie->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete </button>
                    </form>
                </td>
                <td>
                    <a href="{{route('movie.edit',$movie->id)}}" class="btn btn-succes">Edit</a>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$movies->links()}}
    </div>
</body>
</html>