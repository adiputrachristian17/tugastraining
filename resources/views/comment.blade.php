<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                <th>Movie Title</th>
                <th>Comment</th>
                </tr>
            </thead>
            <tbody>
                @foreach($comments as $comment)
                <tr>
                <td>{{$comment->movie->title}}</td>
                <td> {{$comment->komentar}}</td>
                <td>

                    <form action="{{route('comment.delete', $comment->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete </button>
                    </form>
                </td>
                <td>
                    <button class="btn btn-succes">
                    Edit
                    </button>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$comments->links()}}
    </div>
</body>
</html>