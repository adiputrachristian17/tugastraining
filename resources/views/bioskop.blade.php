<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                <th>Movie Title</th>
                <th>Bioskop</th>
                </tr>
            </thead>
            <tbody>
                @foreach($bioskops as $bioskop)
                <tr>
                <td>{{$bioskop->movie->title}}</td>
                <td> {{$bioskop->nama}}</td>
                <td> {{$bioskop->lokasi}}</td>
                <td>
                    <form action="{{route('bioskop.delete', $bioskop->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete </button>
                    </form>
                </td>
                <td>
                    <a href="{{route('bioskop.edit',$bioskop->id)}}" class="btn btn-succes">Edit</a>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$bioskops->links()}}
    </div>
</body>
</html>