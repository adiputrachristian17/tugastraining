<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
<div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{$messages}}</li>
                        @endforeach 
                    </ul>
                </div>
            @endif
            <form action="{{route('movie.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input name="title" type="text" class="form-control" id="title">
            </div>
            <div class="form-group">
                <label for="desc">Synopsis:</label>
                <input name="synopsis" type="text" class="form-control" id="desc">
            </div>
            <div class="form-group">
                <label for="author">Director:</label>
                <input name="director" type="text" class="form-control" id="author">
            </div>
            <div class="form-group">
                <label for="author">Date Realese:</label>
                <input name="daterealese" type="date" class="form-control" id="author">
            </div>
            <div class="form-group">
                <label for="author">Image:</label>
                <input name="image" type="file" class="form-control" id="author">
            </div>
            <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            </form>
            
    </div>
</body>
</html>