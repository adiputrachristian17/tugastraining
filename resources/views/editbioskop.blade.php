<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
</head>
<body>
<div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $messages)
                        <li>{{$messages}}</li>
                        @endforeach 
                    </ul>
                </div>
            @endif
            <form action="{{route('bioskop.update', $bioskop->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('Patch')
            <div class="form-group">
                <label for="title">Nama Bioskop:</label>
                <input name="nama" type="text" class="form-control" value="{{$bioskop->nama}}" id="title">
            </div>
            <div class="form-group">
                <label for="desc">Lokasi:</label>
                <input name="lokasi" type="text" class="form-control" value="{{$bioskop->lokasi}}"  id="desc">
            </div>
            <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            </form>
            
    </div>
</body>
</html>