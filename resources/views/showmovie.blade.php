<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show Movie</title>
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('css/show.css')}}">
</head>
<body>
    <div class="container">
        <div class="d-flex justify-content-center align-item-center">
            <img src="{{asset('storage/image/'. $movie->image)}}" alt="">
        </div>
        <div class="description">
            <p>{{$movie->synopsis}}</p>
        </div>
        <div class="comment">
            <h1>Comment</h1>
            <hr>
            @foreach ($movie->comments as $comment)
            <div class="col-lg-4">
                <div class="content">
                    <p>{{$comment->komentar}}</p>
                </div>
            </div>
            @endforeach
        </div>
        <div class="bioskop">
            <h1> Bioskop </h1>
            <hr>
            @foreach($movie->bioskop as $bioskop)
            <div class="col-lg-4">
                <div class="content">
                    <p>{{$bioskop->nama}}</p>
                    <p>{{$bioskop->lokasi}}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</body>
</html>