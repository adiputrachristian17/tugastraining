<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','WelcomeController@index');
Route::get('/post','PostController@Index');
//Route::get('/','CommentController@index');
Route::get('/comment','CommentController@index');
Route::post('/post','PostController@store')->name('post.add');
//si name ini bebas , tapi mesti samain sama di action di view
Route::get('/movie','MovieController@index');
Route::post('/movieform','MovieController@store')->name('movie.add');
Route::get('/movieform','MovieController@create')->name('movie.create');
Route::get('/movie/show/{id}','MovieController@show')->name('movie.show');
Route::get('/movie/{id}','MovieController@edit')->name('movie.edit');
Route::patch('/movie/{id}','MovieController@update')->name('movie.update');
Route::delete('/movie/{id}','MovieController@delete')->name('movie.delete');
Route::get('/commentform','CommentController@create')->name('comment.create');
Route::post('/commentform','CommentController@store')->name('comment.add');
Route::delete('/comment/{id}', 'CommentController@delete')->name('comment.delete');
Route::get('/bioskop', 'BioskopController@index');
Route::post('/bioskopform','BioskopController@store')->name('bioskop.add');
Route::get('/bioskopform','BioskopController@create')->name('bioskop.create');
Route::get('/bioskop/{id}','BioskopController@edit')->name('bioskop.edit');
Route::patch('/bioskop/{id}','BioskopController@update')->name('bioskop.update');
Route::delete('/bioskop/{id}','BioskopController@delete')->name('bioskop.delete');